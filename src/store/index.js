import Vue from 'vue'
import Vuex from 'vuex'
import router from "@/router/index";
const {getCookie} = require('../functions/cookie');
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    notify:[],
    height:0,
    user:{},
    loader:false,
    sarverPath:'',
  },
  getters: {
  },
  mutations: {
    addNotifyErr(state,msg){
      state.notify.push({
        msg:msg,
        lifeTime:5000,
        color:'red',
      })
    },
    addNotifyAcc(state,msg){
      state.notify.push({
        msg:msg,
        lifeTime:5000,
        color:'green',
      })
    },
    logOut(state){
      state.user = {}
      const cookies = document.cookie.split(";");
      for (let i = 0; i < cookies.length; i++) {
          const cookie = cookies[i];
          const eqPos = cookie.indexOf("=");
          const name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
          document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
      }
      localStorage.token = ''
      localStorage.token_expires_at =''
      localStorage.token_issued_at = ''
      if(router.history.current.path != '/login') router.push('/login')
    },
    setUser(state,user){
      state.user = user
    },
    setRefreshToken(data){
      document.cookie = 'refresh_token='+data.refresh_token
      document.cookie = 'refresh_token_expires_at='+data.refresh_token_expires_at
      document.cookie = 'refresh_token_issued_at='+data.refresh_token_issued_at
    },
    setToken(state, data){
      localStorage.token = data.token
      localStorage.token_expires_at = data.token_expires_at
      localStorage.token_issued_at = data.token_issued_at
      // console.log('da')
      // console.log(localStorage)
    }

  },
  actions: {
  },
  modules: {
  }
})
