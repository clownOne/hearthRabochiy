import Vue from 'vue'
import VueRouter from 'vue-router'
import Auth from '../pages/auth.vue'
import CreateHelp from '../pages/createHelp.vue'
import TableHelps from '../pages/tableHelps.vue'
import UsersTableHelps from '../pages/usersTableHelps.vue'
import Users from '../pages/users.vue'
import Cabinet from '../pages/cabinet.vue'

Vue.use(VueRouter)

const routes = [
  { path: '*', 
    redirect: '/login' 
  },
  {
    path: '/login',
    name: 'login',
    component: Auth
  },
  {
    path: '/create',
    name: 'create',
    component: CreateHelp
  },
  {
    path: '/helps',
    name: 'helps',
    component: TableHelps
  },
  {
    path: '/user-helps',
    name: 'user-helps',
    component: UsersTableHelps
  },
  {
    path: '/users',
    name: 'users',
    component: Users,
  },
  {
    path: '/cabinet',
    name: 'cabinet',
    component: Cabinet,
  }
]

const router = new VueRouter({
  routes
})

export default router
