import axios from "axios";
import {getConfig} from './getConfig'
import {path} from './path'
import store from "@/store/index"
import router from "@/router/index";
async function getAllUsers(){
    try{
        let result;
        console.log(path)
        await axios.get(path+'/api/v1/auth/info/all',await getConfig()).then(res => {
            result = res.data
         });
         return result
     } catch (e){
         console.log(e)
     }
}
async function getAuthUser(token){
    try{
        let result;
        await axios.get(path+`/api/v1/auth/info`,{headers: { Authorization: "Bearer " + token,}}).then(res => {
            result = res.data;
        });
        store.commit('setUser', result)
        return result
     } catch (e){
        console.log(e)
        if(router.name != 'login') router.push('/login')
        console.log('end')
     }
}
async function getUser(Uid, role){
    try{
        let result;
        let id = role && role === 'COORDINATOR' ? '' : '/'+Uid
        console.log(id,role)
        await axios.get(path+`/api/v1/auth/info${id}`,await getConfig()).then(res => {
            result = res.data;
        });
        return result
     } catch (e){
        console.log(e)
         
     }
}
async function getAllMenHelps(){
    const PATH = '/api/v1/help/men/all'
    let result;
    try{
        await axios.get(path+PATH,await getConfig()).then(res => {
            result = res.data
        });
        
        console.log(result)
        for(let i =0;i<result.length;i++){
            result[i].HELPTYPE = 'mens'
        }
        return result
    } catch{

    }

}
async function getHelp(data){
    try{
        let result;
        await axios.get(path+data.path+data.id,await getConfig()).then(res => {
            result = res.data;
        });
        return result
     } catch (e){
        console.log(e)
         
     }
}
export {getAllUsers,getUser,getAuthUser,getAllMenHelps,getHelp}