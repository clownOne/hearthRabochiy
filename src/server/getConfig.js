import router from "@/router/index";
import axios from "axios";
import store from "@/store/index"
import {path} from './path'
import {getAuthUser} from '@/server/get'
const {getCookie} = require('../functions/cookie');
async function getToken(){
    let tokenTimeOld = new Date(localStorage.token_expires_at).getTime()
    let refreshTimeOld = new Date(getCookie('refresh_token_expires_at')).getTime()
    if(tokenTimeOld > Date.now()){
        return localStorage.token
    } else if(refreshTimeOld > Date.now()){
        try{
            let result;
            const head = {headers:{ Authorization: "Bearer " + getCookie('refresh_token')} }
            console.log(head)
            await axios.post(path+'/api/v1/auth/refresh-token', {}, head).then(res => {
                console.log(res);
                result = res;
            });
            console.log(result)
            await getAuthUser(result.data.token)
            store.commit('setToken', result.data)
            return result.data.token
            
        } catch(e){
            console.log(e)
            // console.log(router.name)
            if(router.history.current.path != '/login') router.push('/login')
        }
    } else {
        // console.log(router)
        if(router.history.current.path != '/login') router.push('/login')
    }
    
}
async function getConfig(){
    let token = "Bearer " + await getToken() 
    return {
        headers: { 
            Authorization: token,
            "Content-type": "application/json",
        }
    };
}           
export {getConfig,getToken}